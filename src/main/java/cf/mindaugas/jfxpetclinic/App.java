package cf.mindaugas.jfxpetclinic;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class App extends Application {
    public static void main(String[] args) {
        App.launch();
    }
    @Override
    public void start(Stage primaryStage) {
        System.out.println("Starting app");
        // Creating the container object
        VBox vBox = new VBox();
        // Creating a label control. Constructor argument represents displayed text.
        Label label = new Label("Hello world!");
        // Setting label to be a child of the vBox container
        vBox.getChildren().add(label);
        // Creating a scene. The vBox is passed as it's root.
        Scene scene = new Scene(vBox);
        // Setting the main window's scene.
        primaryStage.setScene(scene);
        // Showing the window.
        primaryStage.show();
    }
}
